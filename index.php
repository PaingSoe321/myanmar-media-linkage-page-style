<!DOCTYPE html>
<html lang="en">
<head>
  <title>Myanmar Media Linkage</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="bootstrap/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="bootstrap/jquery.min.js"></script>
  <script src="bootstrap/bootstrap.min.js"></script>
  
</head>

<body>
  <table border="1" width="90%" height="100%" style="margin:right; background-color: red">
    <nav class="navbar navbar-inverse">


      <div class="collapse navbar-collapse" id="top-navbar-1">
        <div class="navbar-header">
          


          <a class="navbar-brand" href="#"><h1>Myanmar Media Linkage</h1></a>
        </div>

        <div class="navbar-header">
          <a class="navbar-brand" href="#"><font color="black">Media & IT Solutions</a></font>
        </div>

      </div>
    </nav>
  </table>

  <div class="container-fluid"> 
    <section class="items">
    <ul class="nav navbar-nav">
      <li class="active"><a href="#"><span data-hover="HOME">HOME</span></a></li>
    </ul>

    <ul class="nav navbar-nav">
      <li><a href="#"><span data-hover="ABOUT US">ABOUT US</span></a></li>
    </ul>

    <ul class="nav navbar-nav">
      <li><a href="#"><span data-hover="VISION & MISSION">&nbsp;VISION & MISSION</span></a></li>
    </ul>


    <ul class="nav navbar-nav">
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">OUR SERVICES<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">VIDEO PRODUCTION</a></li>
          <li><a href="#">GRAPHIC DESIGN & PRINTING</a></li>
          <li><a href="#">VFX,3D & VISUALIZATION</a></li>
          <li><a href="#">WEB DESIGN & DEVELOPMENT</a></li>
          <li><a href="#">EVENT MANAGEMENT</a></li>
        </ul>
      </li>
    </ul>

    
    <ul class="nav navbar-nav">
      <li><a href="#"><span data-hover="NEWS">&nbsp;NEWS</span></a></li>
    </ul>

    <ul class="nav navbar-nav">
      <li><a href="#"><span data-hover="CONTACT US">&nbsp;CONTACT US</span></a></li>
    </ul>

  </div>
</nav>

<div class="container"> 
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="img/aa.jpg" alt="Los Angeles" style="width:100%;">
      </div>

      <div class="item">
        <img src="img/bb.jpg" alt="Chicago" style="width:100%;">
      </div>

      <div class="item">
        <img src="img/cc.jpg" alt="New york" style="width:100%;">
      </div>

      <div class="item">
        <img src="img/dd.jpg" alt="Japan" style="width:100%;">
      </div>

      <div class="item">
        <img src="img/ee.jpg" alt="Myanmar" style="width:100%;">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>



</div>

<br><br>

<div>
 <p align="center"><em>Copy right 2019 © All rights reserved by Myanmar Media Linkage<br>
  <strong>Evolve</strong>&nbsp;theme by Theme4Press</em></p>
</div>

</div>
</body>
</html>
